package com.janio.s3ftp;

import java.util.ResourceBundle;

public class ConfigBundle {
    private static ResourceBundle resourceBundle ;

    /**
     *
     * @return ResourceBundle
     */
    private static ResourceBundle getInstance(){
        if (resourceBundle == null) {
            resourceBundle = ResourceBundle.getBundle("application");
        }
        return resourceBundle;
    }

    /**
     *
     * @return config value based on key
     */
    public static String getValue(String key) {
        return ConfigBundle.getInstance().getString(key);
    }
}