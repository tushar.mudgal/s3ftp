package com.janio.s3ftp;

import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

/**
 * Before running this Java V2 code example, set up your development environment, including your credentials.
 *
 * For more information, see the following documentation topic:
 *
 * https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/get-started.html
 */

public class S3FtpMain {

    public static void main(String[] args) throws IOException {
        String bucketName = ConfigBundle.getValue("S3_BUCKET");
        StaticCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(
                AwsBasicCredentials.create(
                        ConfigBundle.getValue("S3_ACCESS_KEY"),
                        ConfigBundle.getValue("S3_SECRET_KEY")
                )
        );
        Region region = Region.AP_SOUTHEAST_1;
        S3Client s3 = S3Client.builder()
                .region(region)
                .credentialsProvider(credentialsProvider)
                .build();

//        listBucketObjects(s3, bucketName);
        list(s3, bucketName, ConfigBundle.getValue("S3_PREFIX"));
        s3.close();
    }

    public static void list(S3Client s3Client, String bucketName, String prefix) throws IOException {
        ListObjectsV2Request listObjectsReqManual = ListObjectsV2Request.builder()
                .bucket(bucketName)
                .prefix(prefix)
                .maxKeys(1)
                .build();

        boolean done = false;
        BufferedWriter writer = new BufferedWriter(new FileWriter(ConfigBundle.getValue("WRITE_FILE_PATH")));
        while (!done) {
            ListObjectsV2Response listObjResponse = s3Client.listObjectsV2(listObjectsReqManual);
            for (S3Object content : listObjResponse.contents()) {
                String url = getURL(s3Client, bucketName, content.key());
                writer.write(content.key().split("/")[2].replaceAll(".jpg","").replaceAll(",","-")+","+
                        url);
                writer.newLine();
                System.out.println(content.key().split("/")[2].replaceAll(".jpg",""));
                System.out.println("URL: " + url);
//                cnt++;
            }

            if (listObjResponse.nextContinuationToken() == null) {
                done = true;
            }

            listObjectsReqManual = listObjectsReqManual.toBuilder()
                    .continuationToken(listObjResponse.nextContinuationToken())
                    .build();
        }
        writer.close();
//        int cnt = 0;
//        while (!done) {
//            ListObjectsV2Response listObjResponse = s3Client.listObjectsV2(listObjectsReqManual);
//            for (S3Object content : listObjResponse.contents()) {
//                System.out.println(content.key());
//                System.out.println("URL: " + getURL(s3Client, bucketName, content.key()));
//                cnt++;
//            }
//
//            if (listObjResponse.nextContinuationToken() == null) {
//                done = true;
//            }
//
//            listObjectsReqManual = listObjectsReqManual.toBuilder()
//                    .continuationToken(listObjResponse.nextContinuationToken())
//                    .build();
//        }
//        System.out.println();
//        System.out.println("Total Count: " + cnt);
    }

    // snippet-start:[s3.java2.list_objects.main]
    public static void listBucketObjects(S3Client s3, String bucketName ) {
        try {
            ListObjectsRequest listObjects = ListObjectsRequest
                    .builder()
                    .bucket(bucketName)
                    .prefix(ConfigBundle.getValue("S3_PREFIX"))
                    .build();

            ListObjectsResponse res = s3.listObjects(listObjects);
            List<S3Object> objects = res.contents();
            int cnt = 0;
            for (S3Object myValue : objects) {
                System.out.println("Key: " + myValue.key());
                System.out.println("URL: " + getURL(s3, bucketName, myValue.key()));
                System.out.println();
                cnt++;
            }
            System.out.println("Total Objects Count: " + cnt);
        } catch (S3Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
    }

    public static String getURL(S3Client s3, String bucketName, String keyName ) {

        try {
            GetUrlRequest request = GetUrlRequest.builder()
                    .bucket(bucketName)
                    .key(keyName)
                    .build();

            URL url = s3.utilities().getUrl(request);
            return url.toString();

        } catch (S3Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return null;
    }

    //convert bytes to kbs.
    private static long calKb(Long val) {
        return val/1024;
    }
    // snippet-end:[s3.java2.list_objects.main]
}